(in-package :cl)

(asdf:load-system :mutableconst.irc-logger)

(sb-ext:disable-debugger)

(sb-ext:save-lisp-and-die
 "irc-logger.exe"
 :toplevel #'mutableconst.irc-logger:cli
 :executable t
 :compression 3)
