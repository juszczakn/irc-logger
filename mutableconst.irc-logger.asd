(asdf:defsystem "mutableconst.irc-logger"
  :version "0.0.1"
  :licence "BSD"
  :description ""
  :author "nick.juszczak"
  :long-description
  ""
  :depends-on (;; standard
               :str
               :alexandria
               :bordeaux-threads
               ;; logger
               :sqlite
               :lispibase
               :cl-irc
               :cl-json
               :hunchentoot
               :unix-opts
               ;; qt client
               :drakma
               :qtools
               :qtcore
               :qtgui)
  :components
  ((:module
    "src"
    :serial t
    :components
    ((:file "package")
     (:file "time")
     (:file "database")
     (:file "irc")
     (:file "rest-server")
     (:file "irc-logger")
     ;;qt client
     (:file "client")))))
