(in-package :mutableconst.irc-client)
(named-readtables:in-readtable :qtools)


(define-widget main-window (QWidget)
  ())

(define-subwidget (main-window textview) (q+:make-qtextedit "testing?" main-window)
  (setf (q+:read-only textview) t))

(define-subwidget (main-window lineedit) (q+:make-qlineedit main-window)
  (setf (q+:placeholder-text lineedit) "message..."))

(define-subwidget (main-window hboxlayout) (q+:make-qhboxlayout main-window)
  (let ((vboxlayout (q+:make-qvboxlayout)))
    (q+:add-widget vboxlayout textview)
    (q+:add-widget vboxlayout lineedit)
    (q+:add-layout hboxlayout vboxlayout)))

;; new message entered in lineedit
(define-signal (main-window new-msg) (string))

(define-slot (main-window lineedit) ()
  (declare (connected lineedit (return-pressed)))
  (let ((msg (q+:text lineedit)))
    (q+:clear lineedit)
    (signal! main-window (new-msg string) msg)))

(define-slot (main-window new-msg) ((new-entered-msg string))
  ;; connected to the main window, listening for signal (new-msg:string)
  (declare (connected main-window (new-msg string)))
  (q+:append textview new-entered-msg))


(defun main ()
  (with-main-window (window (make-instance 'main-window))))
