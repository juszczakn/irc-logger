(in-package :mutableconst.irc-logger)

;; bring it all together

(opts:define-opts
  (:name :help
         :description "print help"
         :short #\h
         :long "help")
  (:name :irc-server-url
         :description "irc server url to listen to"
         :short #\u
         :long "irc-server-url"
         :meta-var "URL"
         :arg-parser #'identity)
  (:name :irc-server-port
         :description "irc port"
         :short #\p
         :long "irc-port"
         :meta-var "PORT"
         :arg-parser #'parse-integer)
  (:name :irc-logger-nick
         :description "nick logger will use when joining"
         :short #\n
         :long "irc-logger-nick"
         :meta-var "NICK"
         :arg-parser #'identity)
  (:name :database
         :description "sqlite database file to use, defaults to :memory:"
         :short #\d
         :long "database"
         :meta-var "SQLITE_FILE"
         :arg-parser #'identity)
  ;; not supported yet
  ;; (:name :channels
  ;;        :description "channels to join on startup"
  ;;        :short #\c
  ;;        :long "channels"
  ;;        :meta-var "CHANNELS")
  )


(defun cli ()
  (multiple-value-bind (options free-args)
      (opts:get-opts)
    (declare (ignore free-args))
    (let* ((missing-required-params-p (not (getf options :irc-server-url)))
           (help-p (getf options :help)))
      (when (or missing-required-params-p help-p)
        (opts:describe
         :prefix "Log a given irc server."
         :suffix ""
         :usage-of "irc-logger"
         :args "")))
    (when-let
        ((url (getf options :irc-server-url))
         (port (getf options :irc-server-port))
         (nick (getf options :irc-logger-nick))
         (db (or (getf options :database) ":memory:")))
      (format t "~a ~a ~a ~a" url port nick db)
      (main url port nick :sqlite-db-location db)
      (join-thread *irc-loop-thread*))))


(defun main (irc-server-url irc-server-port irc-logger-nick
             &key
               (sqlite-db-location ":memory:") channels
               (rest-port 4242))
  "Start up that there logger."
  (setf *irc-connection* (create-irc-connection irc-logger-nick irc-server-url irc-server-port))
  (setf *db-connection* (sqlite:connect sqlite-db-location))
  (start-irc-logging :channels-to-join channels)
  (start-rest-server :port rest-port))
