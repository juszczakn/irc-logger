(in-package :cl)

(defpackage :mutableconst.irc-logger
  (:nicknames :irc-logger)
  (:use :cl
        :alexandria
        :bordeaux-threads)
  (:export :*db-connection*
           :*irc-logger-nick*
           :*irc-connection*
           :start-irc-logging
           :stop-irc-logging
           :listen-to-irc-channels
           :start-rest-server
           :stop-rest-server
           :main
           :cli))

(defpackage :mutableconst.irc-client
  (:nicknames :irc-client)
  (:use :cl+qt
        :alexandria
        :bordeaux-threads))
