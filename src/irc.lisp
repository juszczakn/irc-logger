(in-package :mutableconst.irc-logger)


(defvar *irc-connection* nil)

(defvar *irc-loop-thread* nil)


(defun create-irc-connection (irc-logger-nick server &optional port)
  "Convenience function, not really needed."
  (cl-irc:connect :nickname irc-logger-nick
                  :server server
                  :port port))


(defun log-privmsg-hook (msg)
  "Log all messages to channels to the db."
  (let* ((from-nick (slot-value msg 'cl-irc:user))
         (args (slot-value msg 'cl-irc:arguments))
         (channel-name (car args))
         (msg-txt (cadr args))
         (received-time (universal-to-unix-time (slot-value msg 'cl-irc:received-time))))
    (merge-message from-nick msg-txt channel-name
                   :sent-time received-time)))


(defun join-channel-populated-p (channel)
  "Check if channel has users."
  (cl-irc:join *irc-connection* channel)
  (when-let* ((chan (cl-irc:find-channel *irc-connection* channel))
              (user-count (hash-table-size (slot-value chan 'cl-irc:users))))
    (< 0 user-count)))


(defun ignore-channel-restart (condition)
  (declare (ignore condition))
  (invoke-restart 'unable-to-join-channel))


(defun join-channel (channel)
  "Provides restart to ignore joining channel."
  (restart-case (cl-irc:join *irc-connection* channel)
    (unable-to-join-channel () nil)))


(defun maybe-join-channel (channel)
  "Only join channel if populated with some amount of users.
We have to join the channel before being able to determine if
there are users.
Leave right away if there is no one."
  (handler-bind ((cl-irc:no-such-reply #'ignore-channel-restart))
    (join-channel channel)))
  ;;(cl-irc:part *irc-connection* channel)
  ;; (if (join-channel-populated-p channel)
  ;;     (merge-channel channel)
  ;;     (cl-irc:part *irc-connection* channel)))


(defun listen-to-irc-channels (channel-names &aux (res '()))
  "Tell logger to listen to this list of channels, if it's
not already doing so."
  (dolist (channel channel-names)
    (when-let ((joined-p (maybe-join-channel channel)))
      (push channel res)))
  ;; remove nils
  (remove-if #'not res))


(defun init-join-stored-channels ()
  "join all channels stored in db."
  (let ((all-channels (get-all-channel-names)))
    (dolist (channel all-channels)
      (maybe-join-channel channel))))


(defun make-irc-logging-thread-and-loop ()
  "Make a new thread to enter the read-message-loop for irc."
  (let ((irc-connection *irc-connection*))
    (make-thread
     (lambda () (cl-irc:read-message-loop irc-connection))
     :name "irc-logger loop thread")))


(defun stop-irc-logging ()
  "Stop logging and teardown."
  (when *irc-loop-thread* (destroy-thread *irc-loop-thread*))
  (setf *irc-loop-thread* nil)
  (when *irc-connection* (cl-irc:quit *irc-connection*))
  (setf *irc-connection* nil))


(defun start-irc-logging (&key channels-to-join)
  "Initiate irc logging."
  (assert *db-connection*)
  (assert *irc-connection*)
  (assert (not *irc-loop-thread*))
  ;; init
  (init-db-schema)
  (cl-irc:add-hook *irc-connection* 'cl-irc:irc-privmsg-message #'log-privmsg-hook)
  (setf *irc-loop-thread* (make-irc-logging-thread-and-loop))
  (init-join-stored-channels)
  (listen-to-irc-channels channels-to-join))
