(in-package :mutableconst.irc-logger)


(defvar *rest-server* nil)

(defvar *base-url* "/irc-logger/rest/v1/")


(defun make-url (endpoint)
  (str:join "" (list *base-url* endpoint)))


(defun json-decode-w-restart (str)
  "Provides 'bad-json-skip restart."
  (restart-case (cl-json:decode-json-from-string str)
    (bad-json-skip () nil)))


(defun bad-json-skip-restart (condition)
  "Invoke 'bad-json-skip"
  (declare (ignore condition))
  (invoke-restart 'bad-json-skip))


(defun decode-json (str)
  "bad input -> nil"
  (handler-bind ((cl-json:json-syntax-error #'bad-json-skip-restart)
                 (end-of-file #'bad-json-skip-restart))
    (json-decode-w-restart str)))


(defun valid-channel-names-p (strings)
  "check if list of strings look like 'valid' channel names"
  (flet ((valid-strings-p (res s)
           (and res
                (stringp s)
                (str:starts-with-p "#" s)
                (not (cl-ppcre:scan "\\s" s)))))
    (when (and strings (listp strings))
      (reduce #'valid-strings-p strings))))



;;;; handlers


(hunchentoot:define-easy-handler
    (get-messages :uri (make-url "messages")
                  :default-parameter-type 'integer)
    (since-unix-timestamp)
  ;; only let them go back 2 days for now
  (let* ((two-days-ago (- (get-unix-time) (* 86400 2)))
         (since (max since-unix-timestamp two-days-ago)))
    (cl-json:encode-json-to-string (all-messages-since since))))


(hunchentoot:define-easy-handler
    ;;"This shouls really be a post."
    (register-channels :uri (make-url "channels"))
    (names)
  (let* ((names-list (decode-json names)))
    (if (valid-channel-names-p names-list)
        (let ((joined-channels (listen-to-irc-channels names-list)))
          (cl-json:encode-json-to-string
           (plist-hash-table `(:joined ,joined-channels))))
        (cl-json:encode-json-to-string (plist-hash-table '(:joined nil))))))


;;;; control server


(defun start-rest-server (&key (port 4242))
  (assert (not *rest-server*))
  ;; I'd really rather not...
  (setf hunchentoot:*default-content-type* "text/json")
  (setf *rest-server* (make-instance 'hunchentoot:easy-acceptor :port port))
  (hunchentoot:start *rest-server*))

(defun stop-rest-server ()
  (when *rest-server*
    (hunchentoot:stop *rest-server*)
    (setf *rest-server* nil)))
