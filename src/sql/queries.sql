-- db-changeset
-- name: get-channel-by-name
-- author: nick
select channel_id, name from channel where name = :channel_name


-- db-changeset
-- name: get-all-channel-names
-- author: nick
select name from channel


-- db-changeset
-- name: get-all-messages-sent-since
-- author: nick
select
  c.name as channel_name
  ,m.from_nick
  ,m.sent_time
  ,m.message
from
  channel c
join
  message m on c.channel_id = m.channel_id
where
  m.sent_time > :last_contacted_time
order by
  c.channel_id
  ,m.sent_time
  ,m.message_id


-- db-changeset
-- name: merge-channel
-- author: nick
insert or ignore into channel
(
  name,
  created_time
)
values
(
  :name
  ,:created_time
)


-- db-changeset
-- name: merge-message
-- author: nick
insert into message
(
  message
  ,from_nick
  ,channel_id
  ,sent_time
  ,created_time
)
values
(
  :message
  ,:from_nick
  ,:channel_id
  ,:sent_time
  ,:created_time
)
