-- db-changeset
-- name: create channel table
-- author: nick
create table if not exists channel
(
  channel_id integer primary key
  ,name varchar(255) not null
  ,created_time integer not null

  ,constraint unq_channel_name unique (name)
)


-- db-changeset
-- name: create message table
-- author: nick
create table if not exists message
(
  message_id integer primary key
  ,sent_time integer not null
  ,from_nick integer not null
  ,channel_id integer not null
  ,message text not null
  ,created_time integer not null

  ,foreign key (channel_id) references channel (channel_id)
)
