(in-package :mutableconst.irc-logger)


(defvar *db-connection* nil)


(defun read-in-sql (path)
  "Read in sql from path and return hash-table of :name -> :sql."
  (let ((alists (lispibase-core:read-file-to-changesets path))
        (ht (make-hash-table :test #'equal)))
    (dolist (alist alists ht)
      (let ((name (cadr (assoc :name alist)))
            (sql (cadr (assoc :sql alist))))
        (setf (gethash name ht) sql)))))



(defvar *sql* (read-in-sql #p"src/sql/queries.sql"))



;;;; ddl

(defun sqlite-exec-fn (sql &rest params)
  (sqlite:with-transaction *db-connection*
    (apply #'sqlite:execute-non-query *db-connection* sql params)))

(defun sqlite-query-fn (sql &rest params)
  (apply #'sqlite:execute-to-list *db-connection* sql params))


(defvar *changesets*
  (list
   (lispibase-core:read-file-to-changesets #p"src/sql/init.sql")))


(defun init-db-schema ()
  (lispibase-core:init-setup :sqlite :exec-fn #'sqlite-exec-fn :query-fn #'sqlite-query-fn)
  (lispibase-core:try-run-changesets *changesets*))



;;;; sql

(defun get-sql (name)
  (let ((name-key (string-downcase (string name))))
    (gethash name-key *sql*)))


(defun execute (sql-key &rest params)
  (let ((sql (get-sql sql-key)))
    (apply #'sqlite:execute-non-query/named
           `(,*db-connection* ,sql ,@params))))

(defun query (sql-key &rest params)
  (let ((sql (get-sql sql-key)))
    (apply #'sqlite:execute-to-list/named
           `(,*db-connection* ,sql ,@params))))



;; get


(defun get-channel (channel-name)
  (car (query :get-channel-by-name
              ":channel_name" channel-name)))


(defun get-all-channel-names ()
  (mapcar #'car (query :get-all-channel-names)))


(defun get-all-messages-sent-since (last-contacted-time)
  (query :get-all-messages-sent-since ":last_contacted_time" last-contacted-time))


;; merge


(defun merge-channel (channel-name &key (created-time (get-unix-time)))
  (execute :merge-channel
           ":name" channel-name
           ":created_time" created-time))


(defun merge-message (from-nick message to-channel-name
                      &key
                        (sent-time (get-unix-time))
                        (created-time (get-unix-time)))
  (merge-channel to-channel-name)
  (let ((chan-id (car (get-channel to-channel-name))))
    (execute :merge-message
             ":message" message
             ":from_nick" from-nick
             ":channel_id" chan-id
             ":sent_time" sent-time
             ":created_time" created-time)))


;; rest-based


(defun all-messages-since (since)
  "Get all messages sent `since` (unix-timestamp)."
  (flet ((zip (row keys)
           (flatten (mapcar (lambda (a b) (cons a b)) keys row)))
         (new-resizeable-vector ()
           (make-array 100 :fill-pointer 0 :adjustable t)))
    (let ((all-rows (get-all-messages-sent-since since))
          (hash (make-hash-table :test #'equal)))
      (dolist (row all-rows hash)
        (let* ((channel (car row))
               (row-data (plist-hash-table
                          (zip (cdr row) '("nick" "received" "msg"))))
               (vec (ensure-gethash channel hash (new-resizeable-vector))))
          (vector-push row-data vec))))))
